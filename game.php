<?php

class Game
{

    function print_puzzle(){
        echo "Sudoko Puzzle<br>";
        $db = new DbConnection();
        $dbh = $db->get_dbh();
        $bg="cccccc";
        echo "<table border='3' cellpadding='10'>";
        for ($r=1; $r<=9; $r++) {
            echo "<tr>";
            for ($c=1; $c<=9; $c++) {
                $id=$r.$c;
                /* Find the original value for this cell */
                $query="
                    SELECT 
                        orig_value 
                    FROM 
                        cell
                    WHERE 
                        id=$id
                ";
                print "#debug tp GAME query: " . $query . "<br />";
                foreach ($dbh->query($query) as $row) {
                    $orig_value=$row['orig_value'];
                }
                #$result=mysql_query($query)
                #or die(mysql_error());
                #$row=mysql_fetch_array($result);
                #$orig_value=$row['orig_value'];

                print "#debug tp G orig_value: " . $orig_value . "<br />";
                $query="
                    SELECT 
                        value 
                    FROM 
                        cell
                    WHERE 
                        id=$id
                ";
                $result=mysql_query($query)
                or die(mysql_error());
                $row=mysql_fetch_array($result);
                $show=$row['value'];
                
                $font="6";
                if ($orig_value != $show) {
                    /* This is a value which we have determined so
                    we will show it in red */
                    $color="red";
                } else {
                    $color="black";
                }
                
                if (((($c < 4) && ($r < 4)) || (($c > 6) && ($r < 4))) 
                || ((($c > 3) && ($r > 3)) && (($c < 7) && ($r < 7))) 
                || ((($c < 4) && ($r > 6)) || (($c > 6) && ($r > 6)))) {
                    $bg="ffffff";
                } else {
                    $bg="cccccc";
                }
                if ($show == 0) {
                    /* What are the possible values?  */
                    $font="3";
                    $query="
                        SELECT 
                            possible_values 
                        FROM 
                            cell
                        WHERE 
                            id=$id";
                    $result=mysql_query($query)
                    or die(mysql_error());
                    $row=mysql_fetch_array($result);
                    $show=$row['possible_values'];
                    $possible_array=split(":", $show);
                    $pcount=count($possible_array);
                    $hcount=($pcount / 2);
                    $show="";
                    $counter=0;
                    foreach ($possible_array as $p) {
                        $counter++;
                        #if ($counter >= $hcount) {
                        if ($counter > 3) {
                          $counter=0;
                          $show=$show.$p."<br>";
                        } else {
                          $show=$show.$p;
                        }
                    }
                }
                
                echo "<td bgcolor=#".$bg."><p><font size=".$font."><font color=".$color.">".$show."</p></td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
}