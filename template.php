<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php global $title; echo $title; ?></title>
        <link rel="stylesheet" type="text/css" href="Styles/Stylesheet.css" />
    </head>
    <body>
        <div id="wrapper">
            <div id="banner">             
            </div>
            
            <!--
            <nav id="navigation">
                <ul id="nav">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Coffee</a></li>
                    <li><a href="#">Shop</a></li>
                    <li><a href="#">About</a></li>
                </ul>
            </nav>
            -->
            <!-- <div id="main_content"> -->
                <div id="content_area">
                    <?php global $print_puzzle; echo $print_puzzle;  ?>
                    <?php //if($enter_data) echo $enter_data; ?> 

                </div>

                <div id="sidebar">                 
                    <?php global $mini_menu; echo $mini_menu; ?>
                    <?php global $menu; echo $menu; ?>
                </div>
            <!-- </div> -->
           
            <!-- <hr style="clear:both;"> -->
           
           
            <div id="entry_area">
                <?php global $enter_data; if($enter_data) echo $enter_data; ?>
            </div>
           
            <footer>
                <p>All rights reserved</p>
            </footer>
        </div>
    </body>
</html>
