<?php

class Form
{
    function menu() {

        echo "<form method='post' action=$PHP_SELF>";
        echo "---Select Action:<br>";

        echo "<input type='radio' value='new_puzzle' name='request'>Input new puzzle: <br \>";
        echo "<input type='radio' value='solve' name='request'>Solve current puzzle: <br \>";
        echo "<input type='radio' value='guess' name='request'>Solve with Guess Mode: <br \>";    
        echo "<input type='radio' value='reset' name='request'>Reset existing puzzle: <br \>";
        echo "<input type='radio' value='enter' name='request'>Enter data to existing puzzle: <br \>";
        echo "<input type='radio' value='print' name='request'>Print current puzzle: <br \>";
        echo "<input type='radio' value='check_puzzle' name='request'>Check current puzzle: <br \>";

        
        echo " Step Mode: <input type='checkbox' value='1' name='stop'><br />";
        echo " Debug Enabled: <input type='checkbox' value='1' name='debug'><br />";

        echo "  How many data points to enter: <input type='radio' name='quantity' value='1'>1";
        echo "<input type='radio' name='quantity' value='2'>2";
        echo "<input type='radio' name='quantity' value='3'>3";
        echo "<input type='radio' name='quantity' value='4'>4";
        echo "<input type='radio' name='quantity' value='6'>6";
        echo "<input type='radio' name='quantity' value='10'>10<br>";

        echo "<input type='submit' value='Enter'><br>";
        echo "-----------------<br>";
    }   

    function mini_menu() {
        echo "<form method='post' action=$PHP_SELF>";
        echo " Step Mode: <input type='checkbox' value='1' name='stop' checked>";
        echo "<input type='submit' value='Next'><br>";
    }    
}